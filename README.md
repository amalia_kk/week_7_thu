# Docker 🐳

A long time ago, the big companies needed to make sure that their businesses never went down because their business was tech and they relied on it so they'd go buy servers and more and more and so on. They'd over-allocate more than they needed which was an issue because it was v expensive. They needed to do this because as more users came to servers, they needed to scale, in the case where they couldnt- crash and burn. VMware comes out and says we can offer virtualisation. instead of running app and buying a new expensive server, we can allow you to run multiple OSs on the same host. Can run app in isolation on same server/infrastructure but apps are all separate from each other. A lot more efficient. Before, if you wanted to make sure you didnt have a single point of failure, youd place each on different servers and scale horizontally. However, virtualisation is expensive. First, multiple kernels. For every OS added to infr, have to allocate resources for it- virtual hardware, RAM allocation. Now, modern day containerisation- Docker. Great brecause its lightweight, dont have the issue of having crazy to configure AMI thats are hard to set up and deploy. Docker works off images and can create containers from them. Also portable, images are already really small and you can use configuration like Docker Files to spin these things up on pretty much any system. Also its fast- before youd have to boot up OS to get to app and copy files over whereas Docker has all this embedde din conf. 

It's one of many containerisation technologies. Most used. Pulls images from Dockerhub.

Main commands:
```bash
# To start a container using docker run
$ docker run -dp 80:80 docker/getting-started

# -d is for detached mode
# -p is port mapping [outside_container]:[inside_container]

# Check container is running
$ docker ps

# Stop a container
$ docker stop <name_of_container>

# Docker images to see images in a computer
$ docker images 

# ssh into a container
$ docker exec -it <container_name> /bin/bash

# Log into mysql
docker run --name some-mysql2 -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql/mysql-server:latest


```

## Docker with volumes

Docker volumes are shared local disc spaces for containers. 

```bash
# Syntax of docker volumes
$ docker run -v local_directory:path_internal_directory image

# example
docker run -d --name some_mysql2 -e my-secret-password -v /Users/Amalia/Code/Week_7/Friday/Example_volume_2:/var/lib/mysql mysql/mysql-server:latest



```

## Virtual machines

 Setting up = Usually we start with our physical machine, and start to create an EC2 machine: decide on Ubuntu/Red Hat, install Java, need files and stuff then we'd start it. If we want more, we have to repeat all of this process again. Also, for the most part there are lots of unused resources. 


## Containers


An abstraction on top of the physical layer and OS. It's a self-contained environment that runs a system. It allows multiple containers to run inside one VM and one OS. Since it sits above the OS, in theory it's portable to any system/OS. In practice, it's just more portable.

Technically, containers are made up of layers of images. At the base of most containers you'd have Linux based image: Alpine or other. It's important for base images to be small and these base images ensure that containers stay small. On top of base image is application image with intermediate images in between. On top of that is configuration data.

Can be used to run applications, packages up all dependencies and code. They're light, portable and can run across multiple technologies. People usually (wrongly) think they're similar to VMs because they use them to achieve the same goal. However, containers run inside VMs so they can't be the same. 

Usually a VM is used to spin up a system. Containers are used to set up a system such as Nginx or PetClinic (😡). 


## Diff betw containers and VMs

Pimary difference is that containers provide a way to virtualize an OS so that multiple workloads can run on a single OS instance. With VMs, the hardware is being virtualized to run multiple OS instances. Containers are better because of their speed, agility, and portability which make them yet another tool to help streamline software development. 

Applications running on VM system can run different OSs while applications running in a container environment share a single OS.

VM virtualizes the computer system while containers virtualize the operating system only.

VM size is very large while containers take a few seconds to run.

VM uses a lot of system memory while containers require very less memory.

VM is more secure while containers are less secure.

VMs are useful when we require all of OS resources to run various applications while containers are useful when we are required to maximise the running applications using minimal servers.

Examples of VM are: KVM, Xen, VMware while examples of containers are:RancherOS, PhotonOS, Containers by Docker.

Need to set up physical infrastructure and OS whereas container sits on top of these and shares resources.

## Similarities

They are both methods of deploying multiple, isolated services on a single platform.



## Main sections

- Server: It is the docker daemon called dockerd. It can create and manage docker images. Containers, networks, etc.

- Rest API: It is used to instruct docker daemon what to do.

- Command Line Interface (CLI): It is a client which is used to enter docker commands.





## Others

- Kubernetes, EKS, Podman (not containers, orchestration and deployment)
- LDX
- Containerd


## Other notes

Paint image to explain setup of each

## Links

Docker explained in a comic:
https://cloud.google.com/kubernetes-engine/kubernetes-comic

Docker files:
https://docs.docker.com/engine/reference/builder/


## Friday (next day)

Created a script to install httpd. Also created a html file so customise the webpage:

```bash
docker build . -t filipe
# This creates a tag for the image

docker run -dit -p 1234:80 filipe
# Run to create a container and can open localhost:1234 to reveal the webpage

# If we were to create another container using the same tag, we'd have to do
docker run -dit -p 4445:80 --name filipe new_container

# So the syntax is
docker run -dit -p <chosen port>:80 --name <tag> <container name>

```